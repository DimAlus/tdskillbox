// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSkillBoxPlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "../Character/TDSkillBoxCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

ATDSkillBoxPlayerController::ATDSkillBoxPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
}

void ATDSkillBoxPlayerController::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
}

void ATDSkillBoxPlayerController::Tick(float DeltaSeconds){
	Super::Tick(DeltaSeconds);
	this->MovementTick(DeltaSeconds);
}

void ATDSkillBoxPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Started, this, &ATDSkillBoxPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &ATDSkillBoxPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &ATDSkillBoxPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &ATDSkillBoxPlayerController::OnSetDestinationReleased);

		// Setup touch input events
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Started, this, &ATDSkillBoxPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Triggered, this, &ATDSkillBoxPlayerController::OnTouchTriggered);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Completed, this, &ATDSkillBoxPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Canceled, this, &ATDSkillBoxPlayerController::OnTouchReleased);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}

	// Setup movement input events
	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSkillBoxPlayerController::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSkillBoxPlayerController::InputAxisY);
	InputComponent->BindAxis(TEXT("MouseWheel"), this, &ATDSkillBoxPlayerController::InputMouseWheel);
}

void ATDSkillBoxPlayerController::OnInputStarted()
{
	StopMovement();
}

// Triggered every frame when the input is held down
void ATDSkillBoxPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}
}

void ATDSkillBoxPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.f;
}

// Triggered every frame when the input is held down
void ATDSkillBoxPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void ATDSkillBoxPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void ATDSkillBoxPlayerController::InputAxisX(float Value){
	this->AxisX = Value;
}

void ATDSkillBoxPlayerController::InputAxisY(float Value){
	this->AxisY = Value;
}

void ATDSkillBoxPlayerController::InputMouseWheel(float Value){
	ATDSkillBoxCharacter* player = Cast<ATDSkillBoxCharacter>(this->GetCharacter());
	if (player && abs(Value) > 0.1f) {
		player->ChangeCameraZoom(-Value);
	}
}

void ATDSkillBoxPlayerController::MovementTick(float DeltaTime){
	ACharacter* player = this->GetCharacter();
	if (player) {
		player->AddMovementInput(FVector(AxisX, AxisY, 0), 1);

		FVector worldLocation, worldDirection;
		this->DeprojectMousePositionToWorld(worldLocation, worldDirection);	
		float deltaZ = player->GetActorLocation().Z - worldLocation.Z;

		FVector targetLocation = worldLocation + worldDirection * (deltaZ / worldDirection.Z);
		float newRotatorYaw = UKismetMathLibrary::FindLookAtRotation(player->GetActorLocation(), targetLocation).Yaw;
		player->SetActorRotation(FRotator(0, newRotatorYaw, 0));
	}
	
}
//8dde24fe8bf75be6