// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSkillBox.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDSkillBox, "TDSkillBox" );

DEFINE_LOG_CATEGORY(LogTDSkillBox)
 