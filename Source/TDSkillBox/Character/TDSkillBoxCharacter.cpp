// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSkillBoxCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATDSkillBoxCharacter::ATDSkillBoxCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

}

void ATDSkillBoxCharacter::Tick(float DeltaSeconds){
    Super::Tick(DeltaSeconds);

	this->SprintTick(DeltaSeconds);
}

void ATDSkillBoxCharacter::BeginPlay(){
	//// Set default Camera properties
	CameraBoom->TargetArmLength = this->CameraHeight;
	CameraBoom->SetRelativeRotation(this->CameraRotator);
	CameraBoom->bEnableCameraLag = this->CameraIsFlow;
	CameraBoom->CameraLagSpeed = this->CameraSpeed;
	CameraBoom->bDrawDebugLagMarkers = this->CameraIsDrawLag;
	Super::BeginPlay();
}

void ATDSkillBoxCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason){
	Super::EndPlay(EndPlayReason);

	this->GetWorld()->GetTimerManager().ClearTimer(this->CameraHeightTimerHandle);
	this->GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}

void ATDSkillBoxCharacter::ChangeCameraZoom(float deltaZoom){
	float heightStep = this->CameraHeightStep;
	if (this->CameraHeightIsPersents)
		heightStep *= this->CameraHeight / 100.f;
	float newHeight = this->CameraHeight + heightStep * deltaZoom;
	newHeight = FMath::Max(this->CameraMinHeight, FMath::Min(this->CameraMaxHeight, newHeight));
	if (newHeight != this->CameraHeight && !this->CameraHeightTimerHandle.IsValid()) {
		this->GetWorld()->GetTimerManager().SetTimer(this->CameraHeightTimerHandle, this, &ATDSkillBoxCharacter::ChangeCameraZoomTick, 0.01, true);
	}
	this->CameraHeight = newHeight;
	//
}

float GetCameraHeightSpeedCoef(float currentLength, float smoothLength) {
	return currentLength > smoothLength ? 1 : (0.1f + 0.9f * currentLength / smoothLength);
}

void ATDSkillBoxCharacter::ChangeCameraZoomTick(){
	float aim;
	float currentLength = abs(this->CameraHeight - CameraBoom->TargetArmLength);
	float cameraSpeed = this->CameraHeightSpeed * GetCameraHeightSpeedCoef(currentLength, this->CameraHeightSmoothStart);
	// if it is last step
	if (currentLength <= cameraSpeed) {
		aim = this->CameraHeight;
		if (this->CameraHeightTimerHandle.IsValid())
			this->GetWorld()->GetTimerManager().ClearTimer(this->CameraHeightTimerHandle);
	}
	else {
		float aimSign = FMath::Sign(this->CameraHeight - CameraBoom->TargetArmLength);
		aim = aimSign * cameraSpeed + CameraBoom->TargetArmLength;
	}
	CameraBoom->TargetArmLength = aim;
}

float ATDSkillBoxCharacter::GetRunSpeed(){
	if (this->HasMovementState(EMovementState::Sprint_State)) 
		return this->MovementInfo.SprintSpeed;
	if (this->HasMovementState(EMovementState::Aim_State)) 
		return this->MovementInfo.AimSpeed;
	if (this->HasMovementState(EMovementState::Walk_State)) 
		return this->MovementInfo.WalkSpeed;
	return this->MovementInfo.RunSpeed;
}

void ATDSkillBoxCharacter::StartSprint(){
	if (this->Stamina > 0 && !this->HasMovementState(EMovementState::Sprint_State)) {
		this->AddMovementState(EMovementState::Sprint_State);
		this->RemoveMovementState(EMovementState::Aim_State);
		this->RemoveMovementState(EMovementState::Walk_State);
		this->StaminaRestoreCurrentDelay = this->StaminaRestoreDelay;
	}
}

void ATDSkillBoxCharacter::SprintTick(float DeltaSeconds){
	if (this->HasMovementState(EMovementState::Sprint_State)) {
		this->Stamina = FMathf::Max(this->Stamina - this->StaminaSpending * DeltaSeconds, 0);

		float newMovementRotation = this->GetMovementRotation();
		if (this->Stamina <= 0.001 
			|| this->HasMovementState(EMovementState::Aim_State)
			//|| FMath::Abs(FRotator::NormalizeAxis(newMovementRotation - this->SprintRotatorYaw) / DeltaSeconds) > this->MaxSprintRotationSpeed
			|| this->GetMovementComponent()->Velocity.Length() < this->MovementInfo.RunSpeed / 3) {
			// End sprint
			this->RemoveMovementState(EMovementState::Sprint_State);
		}
		this->SprintRotatorYaw = newMovementRotation;
	}
	else if (this->StaminaRestoreCurrentDelay > 0.001) {
		this->StaminaRestoreCurrentDelay = FMathf::Max(this->StaminaRestoreCurrentDelay - DeltaSeconds, 0);
	}
	else if (this->Stamina < this->MaxStamina) {
		this->Stamina = FMathf::Min(this->Stamina + this->StaminaRestore * DeltaSeconds, this->MaxStamina);
	}
}

/// <summary>
/// Get angle between current Velocity and (0, 1, 0) vector
/// </summary>
/// <returns>Angle degree</returns>
float ATDSkillBoxCharacter::GetMovementRotation() {
	FVector& vel = this->GetMovementComponent()->Velocity;
	float angle = FMath::RadiansToDegrees(FMath::Acos(vel.Y / vel.Length()));
	return vel.X < 0 ? 360 - angle : angle;
}


void ATDSkillBoxCharacter::CharacterUpdate(){
	this->GetCharacterMovement()->MaxWalkSpeed = this->GetRunSpeed();
}

void ATDSkillBoxCharacter::AddMovementState(const EMovementState NewMovementState){
	this->MovementStates.Add(NewMovementState);
	this->CharacterUpdate();
}

void ATDSkillBoxCharacter::RemoveMovementState(const EMovementState NewMovementState) {
	this->MovementStates.Remove(NewMovementState);
	this->CharacterUpdate();
}

bool ATDSkillBoxCharacter::HasMovementState(const EMovementState NewMovementState) {
	return this->MovementStates.Contains(NewMovementState);
}
