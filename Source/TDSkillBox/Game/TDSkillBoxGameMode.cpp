// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSkillBoxGameMode.h"
#include "TDSkillBoxPlayerController.h"
#include "../Character/TDSkillBoxCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSkillBoxGameMode::ATDSkillBoxGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSkillBoxPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprint/Character/BP_PlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}