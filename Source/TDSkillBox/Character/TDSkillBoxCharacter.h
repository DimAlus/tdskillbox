// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Lib/Typing.h"
#include "TDSkillBoxCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSkillBoxCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSkillBoxCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;


	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:
	// Camera movement
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera movement")
	bool CameraIsDrawLag = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera movement")
	bool CameraIsFlow = true;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera movement")
	float CameraSpeed = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera movement")
	FRotator CameraRotator = FRotator(0, -80, 0);
	// Camera height
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	float CameraHeight = 1200.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	float CameraMaxHeight = 1500.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	float CameraMinHeight = 300.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	float CameraHeightStep = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	bool CameraHeightIsPersents = true;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	float CameraHeightSpeed = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera height")
	float CameraHeightSmoothStart = 200.f;
	UPROPERTY()
	FTimerHandle CameraHeightTimerHandle;

	UFUNCTION(BlueprintCallable)
	void ChangeCameraZoom(float deltaZoom);
	UFUNCTION()
	void ChangeCameraZoomTick();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
	TSet<EMovementState> MovementStates;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	UFUNCTION(BlueprintCallable)
	float GetRunSpeed();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float Stamina = 100.f;						// Current stamina
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float MaxStamina = 100.f;					// Maximum stamina
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float StaminaSpending = 20.f;				// Stamina spending per second
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float StaminaRestore = 30.f;				// Restore stamina  per second 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float StaminaRestoreDelay = 3.f;			// Delay before stamina resoring
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float StaminaRestoreCurrentDelay = 3.f;		// Current delay before stamina restoring
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float SprintRotatorYaw = 0.f;				// Current sprint rotation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Sprint")
	float MaxSprintRotationSpeed = 90.f;			// Max rotation per second for sprint

	UFUNCTION(BlueprintCallable)
	void StartSprint();
	UFUNCTION(BlueprintCallable)
	void SprintTick(float DeltaSeconds);

	UFUNCTION(BlueprintCallable)
	float GetMovementRotation();

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
	void AddMovementState(const EMovementState NewMovementState);
	UFUNCTION(BlueprintCallable)
	void RemoveMovementState(const EMovementState NewMovementState);
	UFUNCTION(BlueprintCallable)
	bool HasMovementState(const EMovementState NewMovementState);


};

